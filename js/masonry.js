docReady(() => {
    var masonryImages = document.getElementsByClassName("gallery-item");
    var masonryLength = masonryImages.length;


    for(i = 0; i < masonryLength; i++) {
        let item = masonryImages[i];
        item.style.transitionDelay = i * 0.5 + "s";
        item.classList.add("show");
    }



});