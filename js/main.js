/****************************************************************************************
 *           Función que retrasa a execución de JS ata que a web esté lista
 ****************************************************************************************/

function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}


/****************************************************************************************
 *           Programación do reproductor de video
 ****************************************************************************************/

class VideoControls {
    constructor(controlsContainer){
        // Chamamos a función que garda cada elemento de control html como propiedade da clase
        this.asignControls(controlsContainer);
    }
    asignControls(containerID) {
        let controlsCollection = document.getElementById(containerID).children;

        // Percorremos os elementos html que controlan o video engadíndoos a this.controls
        for(let i=0; i<controlsCollection.length; i++) {
            let currentControl = controlsCollection[i];
            if (currentControl.classList.contains('video-control')) {
                let controlType = currentControl.id.split('-').pop();
                this[controlType] = currentControl;
            }
        }
    }
}

class VideoPlayer {
    player; // Player é o elemento html video
    progress = {}; // Almacenará en os dous elementos da progressbar: container e barra
    controls = {}; // Almacenará os botóns que controlan o video
    /*****************************************************
     * this.controls.play
     * this.controls.stop
     * etc.
     ******************************************************/
    constructor(playerID, progressContainerID, controlsContainerID) {
        this.player = document.getElementById(playerID);
        this.progress.container = document.getElementById(progressContainerID);
        this.progress.advance = this.progress.container.children[0];

        // Creamos os controis coa clase VideoControls
        this.controls = new VideoControls(controlsContainerID);

        // Engadimos os listeners e asignamos cada función ao seu control
        this.addListeners();
    }

    addListeners() {
        this.controls.pause.addEventListener('click', ()=>this.pause());
        this.controls.play.addEventListener('click', ()=>this.play());
        this.controls.pause.addEventListener('click', ()=>this.stop());

        this.controls.mute.addEventListener('click', ()=>this.mute());
        this.controls.unmute.addEventListener('click', ()=>this.unmute());

        this.controls.captioning.addEventListener('click', ()=>this.captions());

        this.player.addEventListener('timeupdate', ()=>this.progressUpdate());
    }

    // Abreviamos a chamada as funcións que controlan o elemento vídeo
    pause() { this.player.pause(); }
    play() { this.player.play(); }
    stop() { this.player.pause(); this.player.currentTime = 0; }

    mute() { this.player.muted = true; }
    unmute() { this.player.muted = false;}

    captions() {
        this.player.textTracks[0].mode = 'showing';
    }

    progressUpdate() {
        /*****************************************************************
         * Cuando el vídeo avance actualizamos nuestra barra de progreso
         *****************************************************************/
        this.progress.advance.style.width = `${this.player.currentTime*100/this.player.duration}%`;
    }
}


docReady(()=>{
    var videoPlayer = new VideoPlayer('video-player', 'video-progress-container', 'video-controls');
});