/******************************************************
 * Crea unha función que cando usemos a grid-gallery
 * amose as imaxes cuadradas.
 * Precisarás chamala co evento 'resize' para
 * recalcular o alto cando cambie o tamaño de pantalla
 ******************************************************/

// Función que devolve o ancho do elemento
getElementWidth = (element) => { return element.getBoundingClientRect().width; }

// Función para facer scroll ata un elemento html
scrollToElement = (element) => { element.scrollIntoView({ behavior: 'smooth', block: 'start' });}

window.onload = function () {
    //Seleccionas todos los elementos con clase test
    var menuLi = document.getElementsByClassName('menu-item');
    
    //Recorres la lista de elementos seleccionados
    for (var i = 0; i < menuLi.length; i++) {

        //Añades un evento a cada elemento
        menuLi[i].addEventListener('click', function(e) {

            //Aquí la función que se ejecutará cuando se dispare el evento
            e.preventDefault();
            const dataTarget = this.getAttribute('data-target');
            const offsetTop = document.querySelector(dataTarget).offsetTop;
            
            scroll({
                top: offsetTop,
                behavior: 'smooth'
            });
        });
    }
};